import express from "express";
import * as bodyParser from "body-parser";
import { MainRoutes, TitleRoutes } from "./routes";
import AppConfig from './config/AppConfig';

const mongoose = require('mongoose');
const cors = require('cors');

class App {
	public app : express.Application = express();
	private appRouter : MainRoutes;
	private titlesRouter : TitleRoutes;

	constructor() {
		this.config();
		this.initializeDB();
	}

	private config () : void {
		this.app.use(cors());
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended : false }));

		this.appRouter =  new MainRoutes(this.app);
		this.titlesRouter = new TitleRoutes(this.app);
	}

	private initializeDB() : void {
		(<any>mongoose).Promise = global.Promise;
		mongoose.connect(AppConfig.dbURL, {useNewUrlParser: true});
	}
}

export default new App().app;