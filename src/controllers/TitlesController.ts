import Titles from '../models/TitleSchema';
import { Request, Response, response } from 'express';

export class TitlesController {
	
	public getTitles (req : Request, res: Response) {
		Titles.find({}, (err : Error, titles) => {
			if (err) {
					res.send(err);
			}

			res.status(200).json(titles);
		});
	};

	public getTitlesByName (req : Request, res : Response) {
		Titles.find({TitleName : { $regex : new RegExp(req.query.title, "i") } }, (err : Error, title) => {
			if (err) {
					res.send(err);
			} 
			
			res.status(200).json(title);
		});
	};

	
	public getTitlesByTitleID (req : Request, res : Response) {

		Titles.findOne( { TitleId : parseInt(req.params.id) }, (err : Error, title) => {
			if (err) {
					res.send(err);
			} 
	
			res.status(200).json(title);
		});
		
	};
}