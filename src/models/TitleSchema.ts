import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const TitleSchema = new Schema({
	TitleName : {
		type: String
	},
	TitleNameSortable : {
		type: String
	},
	ReleaseYear : {
		type: String
	},
	Awards : {
		type : Array
	},
	Genres : {
		type : Array
	},
	OtherNames : {
		type : Array
	},
	Participants : {
		type: Array
	},
	Storylines : {
		type: Array
	}
});

const Titles = mongoose.model('Titles', TitleSchema, 'Titles');

export default Titles;