import { Request, Response, Router } from "express";
import { TitlesController } from "../controllers/TitlesController";

export class MainRoutes { 
	constructor (app : Router) {
		app.route('/')
			.get((req: Request, res: Response) => {            
				res.status(200).send({
								message: 'Welcome!'
				})
			});  
	}
}