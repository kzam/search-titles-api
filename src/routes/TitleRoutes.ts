import { Request, Response, Router } from "express";
import { TitlesController } from "../controllers/TitlesController";

export class TitleRoutes { 
    
	public titlesController: TitlesController = new TitlesController(); 
	
		public constructor(app : Router) {   	
		
			// Get All titles 
			app.route('/titles')
				.get(this.titlesController.getTitles);

			// Get title by name
			app.route('/title')
				.get(this.titlesController.getTitlesByName);

			app.route('/title/:id')
				.get(this.titlesController.getTitlesByTitleID);

		}
}