import AppConfig from './config/AppConfig';
import app from './app';

app.listen( AppConfig.PORT, () => {
  console.log(`Server is running http://localhost:${AppConfig.PORT}...`);
});